# EDITAL_016_2021
 
Processo Seletivo com vista à formação de cadastro de reserva destinado à composição de seu
quadro de colaboradores na Área de Desenvolvimento de Tecnologia - Teste, para atuar nos
projetos do CAEd – Centro de Políticas Públicas e Avaliação da Educação.
 
 
# Orientações
Ficamos felizes pelo seu interesse na vaga. Estamos a proucura de um profissional com amplo conhecimento em testes automatizados  em técnicas de qualidade de software. Para avaliar seus conhecimentos, preparamos uma prova dividida em duas etapas. A primeira etapa será composta de algumas perguntas para avaliar conceitos de *QA*. Na segunda etapa avaliaremos de forma prática esses conceitos.
 
## Primeira etapa
Construa um arquivo **.md** ou **.doc** respondendo as perguntas a seguir:
 
1. Estamos no processo final do desenvolvimento de uma *Feature* nova. Foi solicitado a você que avalia a qualidade desse desenvolvimento. Quais ações tomaria para assegurar a qualidade dessa entrega?
 
 
2. Estamos enfrentando várias instabilidades no nosso servidor em determinado horário do dia. Imaginamos que possa ser devido aos acessos simultâneos a nossa aplicação, mas também pode ser problema de configuração do servidor. Como podemos avaliar se o problema está ocorrendo devido a múltiplos acessos a aplicação. Descreve quais técnicas podem ser usadas e quais ferramentas para executá-las.
 
3. Descreve uma **case** de sucesso que teve oportunidade de participar em experiências anteriores.
 
4. Usando **BDD** escreva a história de usuário da segunda etapa da prova. 
 
## Segunda etapa
Construa testes automatizado na linguagem que se sentir mais confortável, vale lembrar que não será aceito testes feitos por ferramenta de gravação de teste. Tente usar o máximo de técnicas possíveis para demonstrar seu conhecimento no assunto.
 
Dado o site da [Fundação caed](http://fundacaocaed.org.br/), faça um mecanismo para validar qual etapa o processo seletivo __Processo Seletivo 016/2021__ encontra-se. Vale ressaltar que o as fases encontra-se na seção de [Processo Seletivo](http://fundacaocaed.org.br/#!/processoseletivos):
   * Editais Abertos
   * Resultados Publicados
   * Editais encerrados

 Encontrando qual seção está o seu edital, pegue a posição(numero) na lista. Com essas informações de qual seção e posição na lista, vamos preencher um [Formulário  do goolge](https://docs.google.com/forms/d/e/1FAIpQLSd94AY1b53-ishJsyuKhoXMiRCrVE6Ai7OeNlwkhjkps6U29A/viewform) com as essa informações.

 Para finalizar, tire um print da tela do [Formulário  do goolge](https://docs.google.com/forms/d/e/1FAIpQLSd94AY1b53-ishJsyuKhoXMiRCrVE6Ai7OeNlwkhjkps6U29A/viewform) preenchida e salve dentro de uma pasta **Print** no seu projeto. Esse passo deve ser automatizado junto do seu teste. 


### Bônus
 
 * **BÔNUS 1:** Use o padrão de projeto *Page-Object*
 * **BÔNUS 2:** Crie/use um mecanismo para que seja executado em mais de cliente esse teste. Exemplo executar no browser com chrome, browser do chrome no android e no browser do chrome no iPhone.
 * **BÔNUS 3:** Crie um arquivo *.md* explicando como você faria se fosse colocar para executar no CI do GitLab

> Lembre-se de nos avisar quais bônus você conseguiu fazer =)

# Avaliação:
* No seu README, você deverá fazer uma explicação sobre a solução encontrada, tecnologias envolvidas e instrução de uso da solução. 

* É interessante que você também registre ideias que gostaria de implementar caso tivesse mais tempo.

* Inclua permissão no seu repositório para romulo.barbosa@caed.ufjf.br
* Envie um email para processoseletivo@fundacaocaed.org.br, até as 18:00 horas do dia 26/03/2021, com assunto **EDITAL_016_2021** informando o término da avaliação.


*Faça com carinho! Não veja somente como uma avaliação, e sim como uma auto-reflexão sobre seus conhecimentos, mostre-nos o quanto você é bom e especial! Os seus resultados é o maior portfolio de quem voce é.*


> Em caso de dúvida, entre em contato conosco.